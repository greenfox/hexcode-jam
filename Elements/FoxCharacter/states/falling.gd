extends EmptyState

class_name Falling

export var coyoteTime = 0.2
export var fallHardness = 2.0
var stateTime = 0


var hardfall = false

func physicsProcess(character:FoxCharacter):
	stateTime += character.delta

	var vVel := character.verticalVelocity
	var hVel := character.velocity - vVel

	var gameFeelGravity := character.gravity*character.delta
	
#	print({vVel=vVel,grav=character.gravity,cross=})
	var downFall :bool= character.gravity.dot(vVel) > 0
	
	if downFall or hardfall or character.controls.jumpReleased:
		hardfall = true
		gameFeelGravity *= fallHardness
	
	character.velocity += gameFeelGravity
	
	if character.controls.direction.x != 0:
		var rightVector = Vector2(1,0).rotated(character.rotation)
		var hSpeed = character.velocity.project(rightVector)
		var vSpeed = character.velocity - hSpeed
		hSpeed += rightVector * character.controls.direction.x * character.delta * character.airControl
		hSpeed = hSpeed.clamped(character.maxRun)
		character.velocity = hSpeed + vSpeed

	character.defualtMoveAndSlide()

	if character.controls.jumpPressed and stateTime < coyoteTime:
		print("coyoteTime jump!") #todo special effect at jump point
		makeCurrentState(self)
		
		character.velocity -= character.gravity.normalized() * character.jumpVelocity + character.velocity.project(character.gravity)

	if character.is_on_floor():
		get_parent().setState("walking")


	if character.controls.direction.x < 0:
		character.get_node("fox").flip_h = true
	if character.controls.direction.x > 0:
		character.get_node("fox").flip_h = false



func makeCurrentState(oldstate):
	stateTime = 0
	if oldstate:
		if oldstate.has_method("getJumpedThisFrame"):
			if oldstate.getJumpedThisFrame():
				stateTime = INF
	hardfall = false
	emit_signal("playAnimation","jump")
