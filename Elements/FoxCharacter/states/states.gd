extends Node

export var currentState:NodePath


signal playAnimation(animation)

func _ready():
	var a = get_node(currentState)
	a.makeCurrentState(null)
	for i in get_children():
		i.connect("playAnimation",self,"_playAnimation")


func physicsProcess(character:FoxCharacter):
	get_node(currentState).physicsProcess(character)


func setState(newStateName):
	print("state changed!:",newStateName)
	var oldState = get_node(currentState)
	currentState = NodePath(newStateName)
	get_node(currentState).makeCurrentState(oldState)


func _playAnimation(animation):
	emit_signal("playAnimation",animation)


func playAnimation(animation):
	pass # Replace with function body.
