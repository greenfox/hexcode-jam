extends Node

class_name EmptyState

signal playAnimation(animation)


func physicsProcess(object):
	print_debug("EmptyState.physicsProcess should not be called")

func setState(newState):
	get_parent().setState(newState)


func makeCurrentState(oldState):
	pass
