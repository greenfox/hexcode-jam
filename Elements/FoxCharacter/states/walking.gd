extends EmptyState

class_name Walking



"""
TODO: all this:

character controller should accelerate quickly (0-100% in one frame? or more floaty?)
character should deccelerate faster than that^
turning arround should apply your decel speed until you stop, then apply accelerate speed
you should not go faster than a max speed

everything should be impected by delta

"""

var jumpedThisFrame = false
func getJumpedThisFrame()->bool:
	return jumpedThisFrame

func physicsProcess(c:FoxCharacter):
	jumpedThisFrame = c.controls.jumpPressed
	if jumpedThisFrame:
		pass
	c.velocity += c.gravity*c.delta
	
	var speedDelta# = Vector2(c.controls.direction.x * c.moveAccel*c.delta,0).rotated(c.rotation)
	if c.controls.direction.x == 0:
		speedDelta = -c.velocity.normalized()*c.delta*c.moveDecel
		if speedDelta.length() > c.velocity.length():
			c.velocity = Vector2.ZERO
			speedDelta = Vector2.ZERO
			emit_signal("playAnimation","idle")
	else:
		speedDelta = Vector2(c.controls.direction.x * c.moveAccel*c.delta,0).rotated(c.rotation)
		emit_signal("playAnimation","walk")
	c.velocity += speedDelta
	
	var rightVector = Vector2(1,0).rotated(c.rotation)
	var sideSpeed = c.velocity.project(Vector2(1,0).rotated(c.rotation))
	var vertSpeed = c.velocity - sideSpeed
	
	sideSpeed = sideSpeed.clamped(c.maxRun)
	

	if c.controls.jumpPressed:
		vertSpeed -= c.gravity.normalized() * c.jumpVelocity

	
	c.velocity = sideSpeed + vertSpeed
	

	c.defualtMoveAndSlide()
	if c.is_on_floor():
		pass
	else:
		setState("falling")

	var rotatedV = c.velocity.rotated(-c.rotation)
	if rotatedV.x < 0:
		c.get_node("fox").flip_h = true
	if rotatedV.x > 0:
		c.get_node("fox").flip_h = false


