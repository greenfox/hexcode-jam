extends KinematicBody2D

class_name FoxCharacter

# https://twitter.com/mattthorson/status/1238338574220546049?lang=en

var controls:ControlsCapture
export var active:=false


export var velocity:Vector2

export var gravityMult = 1

export var maxRun:float
export var moveAccel:float=0
export var moveDecel:float=0
export var jumpVelocity:float=0
export var airControl:float=0


var gravity:Vector2# setget ,fixGravity
var gravDirty := false


var verticalVelocity:Vector2 setget , getVertVel



var delta:float

func _ready():
	pass


func _physics_process(delta_frame):
	gravDirty = true
	fixGravity()
	delta = delta_frame
	controls = $controls.updateControls() if active else $controls.clearControls()
	var targetRotation = gravity.angle() - (PI/2)
	rotation = targetRotation

	$states.physicsProcess(self)
	
#	var rotatedV = velocity.rotated(-rotation)
#	if rotatedV.x < 0:
#		$fox.flip_h = true
#	if rotatedV.x > 0:
#		$fox.flip_h = false

#	print($AnimationPlayer.current_animation)
	
func defualtMoveAndSlide():
	var down:Vector2 = -gravity.normalized()
	velocity = move_and_slide_with_snap(velocity,down*250,down,false,4,deg2rad(60),false)


func _on_states_playAnimation(animation,force=false):
	if $AnimationPlayer.current_animation != animation or force:
		$AnimationPlayer.play(animation)


var gravSources = {}



#spin gravity
func fixGravity():
	var prio = -INF
	if gravDirty:
		gravity = Vector2.ZERO
		for g in gravSources:
			if prio < g.priority:
				gravity = g.calcGravDirection(global_position)
			elif prio == g.priority:
				gravity += g.calcGravDirection(global_position)
		gravity = gravity * gravityMult
		if gravity == Vector2.ZERO:
			gravity = gravityMult * ProjectSettings.get("physics/2d/default_gravity_vector") * ProjectSettings.get("physics/2d/default_gravity") 
		gravDirty = false
	#todo add catch for zero gravity
	return gravity





func addGrav(value):
	gravDirty = true
	gravSources[value] = value
	
func removeGrav(value):
	gravDirty = true
	gravSources.erase(value)



func getVertVel()->Vector2:
	return velocity.project(Vector2(0,1).rotated(rotation))

