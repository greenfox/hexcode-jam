extends Node

class_name ControlsCapture



var direction = Vector2.ZERO
var normalized = Vector2.ZERO
var jumpPressed = false
var jumpReleased = false
var contextPressed = false

func updateControls()->ControlsCapture:
	
	direction = Vector2.ZERO
	if Input.is_action_pressed("up"):
		direction += Vector2.UP
	if Input.is_action_pressed("down"):
		direction += Vector2.DOWN
	if Input.is_action_pressed("left"):
		direction += Vector2.LEFT
	if Input.is_action_pressed("right"):
		direction += Vector2.RIGHT

	contextPressed = Input.is_action_just_pressed("contextButton")
		

	normalized = direction.normalized()

	jumpPressed = Input.is_action_just_pressed("up")
	jumpReleased = Input.is_action_just_released("up")

	return self
	
func clearControls()->ControlsCapture:
	direction = Vector2.ZERO
	normalized = Vector2.ZERO
	jumpPressed = false
	jumpReleased = false
	contextPressed = false
	return self


func _to_string():
	return JSON.print({
		d=direction,
		j=jumpPressed
	})
