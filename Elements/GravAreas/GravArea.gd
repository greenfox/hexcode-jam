extends Area2D

func _on_GravArea_body_entered(body):
	if body.has_method("addGrav"):
		body.addGrav(self)
	pass # Replace with function body.


func _on_GravArea_body_exited(body):
	if body.has_method("removeGrav"):
		body.removeGrav(self)
	pass # Replace with function body.


func calcGravDirection(point:Vector2)->Vector2:
	if gravity_point:
		var reletive:Vector2 = (global_position - point ).normalized()
		return reletive * gravity
	else:
		return gravity_vec * gravity

#	return Vector2.ZERO
