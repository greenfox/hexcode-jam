tool
extends Camera2D

export var target:NodePath setget retarget
export var rotateSpeed:float=0.1

var a :RemoteTransform2D= null

func _ready():
	retarget(target)


func _process(delta):
	pass
	var t:Node2D= get_node(target)
	rotation = lerp_angle(rotation,t.rotation,delta/rotateSpeed)


func retarget(value):
	if a != null:
		a.queue_free()
		a= null
	target = value
	if get_node(target) == null:
		return
	a = RemoteTransform2D.new() #todo cleanup if this moves at some point
	a.update_rotation = false
	get_node(target).add_child(a)
	a.remote_path = get_path()
