extends Node


var colors = [
	Color("#2b0f54"),
	Color("#ab1f65"),
	Color("#ff4f69"),
	Color("#fff7f8"),
	Color("#ff8142"),
	Color("#ffda45"),
	Color("#3368dc"),
	Color("#49e7ec")
]

var phyFrame = 0;

func _physics_process(delta):
	phyFrame += 1;
